let colorOn;
let colorOff;
let colorOver
let time = 0;
let timeView = 0;
let colorList = [];
let dipList = [];
let nodeList = [];

const unit = 1;
const widthDisplay = 200;
const heightDisplay = 225;
const pinDimension = 10;

function setup() {
  createCanvas(1920, 1080);
  background(200);
  creatColor();
  createCD40110();
  createCD4060();
  createCD4027();
  createDisplay();
  createResistor();

  createNodeList();
}

function draw() {
  timer();
  showDip();
  showDisplay();
  showResistor();
}

//---------------------------//
function creatColor() {
  colorOn = color(255, 0, 0);
  colorOff = color(25, 0, 0);
  colorOver = color(0, 255, 0);
  colorList.push(color(0, 0, 0));
  colorList.push(color(100, 25, 0)); //51,25,0
  colorList.push(color(255, 0, 0));
  colorList.push(color(255, 119, 0));
  colorList.push(color(255, 255, 0));
  colorList.push(color(0, 255, 0));
  colorList.push(color(0, 0, 255));
  colorList.push(color(102, 0, 102));
  colorList.push(color(96, 96, 96));
  colorList.push(color(0, 0, 0));
}

function timer() {
  if (time > 100) {
    time = 0;
  }
  time++;
  timeView = time / 10;
}

//-----------------------------//
function createCD40110() {
  for (let i = 0; i < 6; i++) {
    let xc = new Dip(16, 50 + (150 * i), 100, "CD40110");
    xc.pinList[0].pinName = (i + 1) + "_a";
    xc.pinList[1].pinName = (i + 1) + "_g";
    xc.pinList[2].pinName = (i + 1) + "_f";
    xc.pinList[3].pinName = "TOGGLE_ENABLE";
    xc.pinList[4].pinName = "RESET";
    xc.pinList[5].pinName = "LATCH_ENABLE";
    xc.pinList[6].pinName = (i + 1) + "_CLOCK_DOWN";
    xc.pinList[7].pinName = "VSS";
    xc.pinList[8].pinName = (i + 1) + "_CLOCK_UP";
    xc.pinList[9].pinName = "CARRY";
    xc.pinList[10].pinName = "BORROW";
    xc.pinList[11].pinName = (i + 1) + "_e";
    xc.pinList[12].pinName = (i + 1) + "_d";
    xc.pinList[13].pinName = (i + 1) + "_c";
    xc.pinList[14].pinName = (i + 1) + "_b";
    xc.pinList[15].pinName = "VDD";
    dipList.push(xc);
  }
}

function createCD4060() {
  for (let i = 0; i < 1; i++) {
    let xc = new Dip(16, 50, 300, "CD4060");
    xc.pinList[0].pinName = (i + 1) + "_Q12";
    xc.pinList[1].pinName = (i + 1) + "_Q13";
    xc.pinList[2].pinName = (i + 1) + "_Q14";
    xc.pinList[3].pinName = (i + 1) + "_Q6";
    xc.pinList[4].pinName = (i + 1) + "_Q5";
    xc.pinList[5].pinName = (i + 1) + "_Q7";
    xc.pinList[6].pinName = (i + 1) + "_Q4";
    xc.pinList[7].pinName = "VSS";
    xc.pinList[8].pinName = (i + 1) + "_";
    xc.pinList[9].pinName = (i + 1) + "_";
    xc.pinList[10].pinName = (i + 1) + "_";
    xc.pinList[11].pinName = (i + 1) + "_RESET";
    xc.pinList[12].pinName = (i + 1) + "_Q9";
    xc.pinList[13].pinName = (i + 1) + "_Q8";
    xc.pinList[14].pinName = (i + 1) + "_Q10";
    xc.pinList[15].pinName = "VDD";
    dipList.push(xc);
  }
}

function createCD4027() {
  for (let i = 0; i < 1; i++) {
    let xc = new Dip(16, 200, 300, "CD4027");
    xc.pinList[0].pinName = (i + 1) + "_Q2";
    xc.pinList[1].pinName = (i + 1) + "_/Q2";
    xc.pinList[2].pinName = (i + 1) + "_CLOCK2";
    xc.pinList[3].pinName = (i + 1) + "_REESET2";
    xc.pinList[4].pinName = (i + 1) + "_K2";
    xc.pinList[5].pinName = (i + 1) + "_J2";
    xc.pinList[6].pinName = (i + 1) + "_SET2";
    xc.pinList[7].pinName = "VSS";
    xc.pinList[8].pinName = (i + 1) + "_SET1";
    xc.pinList[9].pinName = (i + 1) + "_J1";
    xc.pinList[10].pinName = (i + 1) + "_K1";
    xc.pinList[11].pinName = (i + 1) + "_RESET1";
    xc.pinList[12].pinName = (i + 1) + "_CLOCK1";
    xc.pinList[13].pinName = (i + 1) + "_/Q1";
    xc.pinList[14].pinName = (i + 1) + "_Q1";
    xc.pinList[15].pinName = "VDD";
    dipList.push(xc);
  }
}

function createDisplay() {
  displayTenHour = new SevenSegment(50, 600, "displayTenHour");
  displayHour = new SevenSegment(250, 600, "displayHour");
  displayTenMinute = new SevenSegment(450, 600, "displayTenMinute");
  displayMinute = new SevenSegment(650, 600, "displayMinute");
  displayTenSecond = new SevenSegment(850, 600, "displayTenSecond");
  displaySecond = new SevenSegment(1050, 600, "displaySecond");
}

function createResistor() {
  resistorList = [];
  r1 = new Resistor(1400, 100, 10000);
  resistorList.push(r1);
  r2 = new Resistor(1400, 150, 4700);
  resistorList.push(r2);
  r3 = new Resistor(1400, 200, 1000);
  resistorList.push(r3);
  r4 = new Resistor(1400, 250, 4700);
  resistorList.push(r4);
  r5 = new Resistor(1400, 300, 10000);
  resistorList.push(r5);
  r6 = new Resistor(1400, 350, 4700);
  resistorList.push(r6);
  r7 = new Resistor(1400, 400, 10000);
  resistorList.push(r7);
  r8 = new Resistor(1400, 450, 4700);
  resistorList.push(r8);
  r9 = new Resistor(1400, 500, 10000);
  resistorList.push(r9);
  r10 = new Resistor(1400, 550, 4700);
  resistorList.push(r10);
  r11 = new Resistor(1400, 600, 10000);
  resistorList.push(r11);
  r12 = new Resistor(1400, 650, 4700);
  resistorList.push(r12);
  r13 = new Resistor(1400, 700, 220000);
  resistorList.push(r13);
  r14 = new Resistor(1600, 100, 10000000);
  resistorList.push(r14);
  r15 = new Resistor(1600, 150, 91); //42x
  resistorList.push(r15);
  r16 = new Resistor(1600, 200, 10000);
  resistorList.push(r16);
  r17 = new Resistor(1600, 250, 10000);
  resistorList.push(r17);
  r18 = new Resistor(1600, 300, 10000);
  resistorList.push(r18);
  r19 = new Resistor(1600, 350, 4700);
  resistorList.push(r19);
  r20 = new Resistor(1600, 400, 10000);
  resistorList.push(r20);
  r21 = new Resistor(1600, 450, 10000);
  resistorList.push(r21);
  r22 = new Resistor(1600, 500, 10000);
  resistorList.push(r22);
  r23 = new Resistor(1600, 550, 150);
  resistorList.push(r23);
  r24 = new Resistor(1600, 600, 150);
  resistorList.push(r24);
  r25 = new Resistor(1600, 650, 360);
  resistorList.push(r25);
  r26 = new Resistor(1600, 700, 360);
  resistorList.push(r26);
}

//---------------------------------//
function showDip() {
  for (let i = 0; i < dipList.length; i++) {
    dipList[i].show();
  }
}

function showDisplay() {
  displayTenHour.displayNumber(timeView, false);
  displayTenHour.show();
  displayHour.displayNumber(timeView, true);
  displayHour.show();
  displayTenMinute.displayNumber(timeView, false);
  displayTenMinute.show();
  displayMinute.displayNumber(timeView, true);
  displayMinute.show();
  displayTenSecond.displayNumber(timeView, false);
  displayTenSecond.show();
  displaySecond.displayNumber(timeView, false);
  displaySecond.show();
}

function showResistor() {
  for (let i = 0; i < resistorList.length; i++) {
    resistorList[i].show();
  }
}

function createNodeList() {
  print("Start");
  for (let i = 0; i < dipList.length; i++) {
    print("dipList");
    for (let j = 0; j < dipList[i].pinList.length; j++) {
      print("dipList");
      for (let k = 0; k < nodeList.length + 1; k++) {
        print("NodeList");
        if (dipList[i].pinList[j].pinName != nodeList[k]) {
          nodeList.push(dipList[i].pinList[j].pinName);
          print(dipList[i].pinList[j].pinName);
          break;
        }
      }
    }
  }
}
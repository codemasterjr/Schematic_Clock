class LED {
  constructor() {
    var state;
    this.state = 1;
  }
  show() {
    if (this.state == 1) {
      fill(255, 0, 0);
    } else {
      {
        fill(25, 0, 0);
      }
    }
    stroke(64, 0, 0);
    ellipseMode(CENTER)

    ellipse(0, 0, 15);
  }
}
class Resistor {
  constructor(xPos, yPos, rValue, rPrecision) {
    this.x = xPos;
    this.y = yPos;
    this.rWidth = 75;
    this.rHeight = 25;
    this.pinDim = pinDimension
    this.value = rValue;
    this.precision = rPrecision;
    this.numberDigit = 0;
    if (this.value < 10) {
      this.numberDigit = 1;
    } else if (this.value < 100) {
      this.numberDigit = 2
    } else if (this.value < 1000) {
      this.numberDigit = 3
    } else if (this.value < 10000) {
      this.numberDigit = 4
    } else if (this.value < 100000) {
      this.numberDigit = 5
    } else if (this.value < 1000000) {
      this.numberDigit = 6
    } else if (this.value < 10000000) {
      this.numberDigit = 7
    } else if (this.value < 100000000) {
      this.numberDigit = 8
    } else if (this.value < 1000000000) {
      this.numberDigit = 9
    } else if (this.value < 10000000000) {
      this.numberDigit = 10
    }
    this.strip1 = colorList[Math.trunc(this.value / (Math.pow(10, (this.numberDigit - 1))))];
    this.strip2 = colorList[Math.trunc(this.value / (Math.pow(10, this.numberDigit - 2))) - (Math.trunc(this.value / (Math.pow(10, (this.numberDigit - 1)))) * 10)];
    this.strip3 = colorList[this.numberDigit - 2];
    this.strip4 = colorList[0];
    this.pin1 = new Pin(1, this.x - (this.rWidth / 2) - (this.pinDim / 2), this.y);
    this.pin2 = new Pin(2, this.x + (this.rWidth / 2) + (this.pinDim / 2), this.y);
  }
  show() {
    rectMode(CENTER);
    noStroke();
    fill(255, 204, 153);
    rect(this.x, this.y, this.rWidth, this.rHeight);
    fill(this.strip1);
    rect(this.x - 15, this.y, 5, 25)
    fill(this.strip2);
    rect(this.x - 5, this.y, 5, 25)
    fill(this.strip3);
    rect(this.x + 5, this.y, 5, 25)
    fill(this.strip4);
    rect(this.x + 15, this.y, 5, 25)
    this.pin1.show();
    this.pin2.show();
  }

}
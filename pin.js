class Pin {
  constructor(tempPinNumber, posX, posY) {
    this.pinDim = pinDimension
    this.state = 0;
    this.x = posX;
    this.y = posY;
    this.pinNumber = tempPinNumber;
    this.pinName;
  }

  show() {
    noStroke();
    this.mouseOver();
    if (this.state == 1) {
      fill(0, 255, 0);
    } else {
      fill(100);
    }
    rectMode(CENTER);
    rect(this.x, this.y, this.pinDim, this.pinDim);
  }

  mouseOver() {
    if (mouseX > this.x - (this.pinDim / 2) && mouseX < this.x + (this.pinDim / 2) && mouseY > this.y - (this.pinDim / 2) && mouseY < this.y + (this.pinDim / 2)) {
      this.state = 1;
      print(this.pinName);

    } else {
      this.state = 0;
    }
  }
}
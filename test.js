var dip1;

function setup() {
  createCanvas(1920, 1080);
  background(200);
  colorOn = color(255, 0, 0);
  colorOff = color(25, 0, 0);
  colorOver = color(0, 255, 0);
  dip1 = new Dip(8, 50, 100, "TEST");
}

function draw() {
  dip1.show();
}

var offset = 20;
class Dip {
  constructor(numberOfPin, posX, posY, name) {
    var pinCount;
    var dipHeight;
    var dipWidth;
    var name;
    this.name = name;
    this.x = posX;
    this.y = posY;
    this.pinCount = numberOfPin;
    this.dipWidth = 80;
    this.dipHeight = ((this.pinCount / 2) * (pinDim * 2)) + (offset * 2) - pinDim;
  }
  show() {
    rectMode(CORNER);
    noStroke();
    fill(50);
    rect(this.x, this.y, this.dipWidth, this.dipHeight);
    fill(80);
    arc(this.x + (this.dipWidth / 2), this.y, this.dipWidth / 2, this.dipWidth / 2, 0, PI);
    //These two loop juste draw the pin
    //I want to have acces to all the pin object
    for (var i = 0; i < this.pinCount / 2; i++) {
      var pin = new Pin(i, pin_in, this.x - (pinDim / 2), this.y + offset + (pinDim / 2) + (i * pinDim * 2));
      pin.show();
    }
    for (var i = 0; i < this.pinCount / 2; i++) {
      var pin = new Pin(i, pin_in, this.x + this.dipWidth + (pinDim / 2), this.y + offset + (pinDim / 2) + (i * pinDim * 2));
      pin.show();
    }
  }
}
//-----------------------------pin.js
//I want to have acces to the position of each pin I have created.
var pinDim = 10;
class Pin {
  constructor(tempPinNumber, pinType, posX, posY) {
    var pinNumber
    var state;
    this.state = 0;
    this.x = posX;
    this.y = posY;
    pinNumber = tempPinNumber;
  }
  show() {
    noStroke();
    fill(100);
    rectMode(CENTER);
    rect(this.x, this.y, pinDim, pinDim);
  }
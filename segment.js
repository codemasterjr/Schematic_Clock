var lenghtSegment = 80;
var widthSegment = 15;
class Segment {

  constructor() {
    var state;
    this.state = 0;
  }
  show() {
    if (this.state == 1) {
      fill(255, 0, 0);
    } else {
      {
        fill(25, 0, 0);
      }
    }
    strokeWeight(2);
    stroke(64, 0, 0);
    beginShape();
    vertex(-(lenghtSegment / 2), 0);
    vertex(-(lenghtSegment / 2) + (widthSegment / 2), widthSegment / 2);
    vertex((lenghtSegment / 2) - (widthSegment / 2), widthSegment / 2);
    vertex((lenghtSegment / 2), 0);
    vertex((lenghtSegment / 2) - (widthSegment / 2), -(widthSegment / 2));
    vertex(-(lenghtSegment / 2) + (widthSegment / 2), -(widthSegment / 2));
    endShape(CLOSE);
  }
}
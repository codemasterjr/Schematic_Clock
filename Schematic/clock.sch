EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74LS48 U?
U 1 1 5A526BB0
P 28600 -3500
F 0 "U?" H 28600 -3400 50  0000 C CNN
F 1 "74LS48" H 28600 -3600 50  0000 C CNN
F 2 "" H 28600 -3500 50  0001 C CNN
F 3 "" H 28600 -3500 50  0001 C CNN
	1    28600 -3500
	1    0    0    -1  
$EndComp
$Comp
L 74LS48 U?
U 1 1 5A526F69
P 28600 -2650
F 0 "U?" H 28600 -2550 50  0000 C CNN
F 1 "74LS48" H 28600 -2750 50  0000 C CNN
F 2 "" H 28600 -2650 50  0001 C CNN
F 3 "" H 28600 -2650 50  0001 C CNN
	1    28600 -2650
	1    0    0    -1  
$EndComp
$Comp
L 74LS48 U?
U 1 1 5A527133
P 28600 -1800
F 0 "U?" H 28600 -1700 50  0000 C CNN
F 1 "74LS48" H 28600 -1900 50  0000 C CNN
F 2 "" H 28600 -1800 50  0001 C CNN
F 3 "" H 28600 -1800 50  0001 C CNN
	1    28600 -1800
	1    0    0    -1  
$EndComp
$Comp
L 74LS48 U?
U 1 1 5A52714D
P 28600 -950
F 0 "U?" H 28600 -850 50  0000 C CNN
F 1 "74LS48" H 28600 -1050 50  0000 C CNN
F 2 "" H 28600 -950 50  0001 C CNN
F 3 "" H 28600 -950 50  0001 C CNN
	1    28600 -950
	1    0    0    -1  
$EndComp
$Comp
L 74LS48 U?
U 1 1 5A5273E1
P 28600 -100
F 0 "U?" H 28600 0   50  0000 C CNN
F 1 "74LS48" H 28600 -200 50  0000 C CNN
F 2 "" H 28600 -100 50  0001 C CNN
F 3 "" H 28600 -100 50  0001 C CNN
	1    28600 -100
	1    0    0    -1  
$EndComp
$Comp
L 74LS48 U?
U 1 1 5A5273E7
P 28600 750
F 0 "U?" H 28600 850 50  0000 C CNN
F 1 "74LS48" H 28600 650 50  0000 C CNN
F 2 "" H 28600 750 50  0001 C CNN
F 3 "" H 28600 750 50  0001 C CNN
	1    28600 750 
	1    0    0    -1  
$EndComp
$Comp
L 7SEGMENT_CC U?
U 1 1 5A528B45
P 30050 1600
F 0 "U?" H 29550 1975 50  0000 R CNN
F 1 "7SEGMENT_CC" H 29550 1900 50  0000 R CNN
F 2 "" H 30100 1300 50  0001 L CNN
F 3 "" H 30050 1620 50  0001 L CNN
	1    30050 1600
	1    0    0    -1  
$EndComp
$Comp
L 7SEGMENT_CC U?
U 1 1 5A528DFC
P 31000 1600
F 0 "U?" H 30500 1975 50  0000 R CNN
F 1 "7SEGMENT_CC" H 30500 1900 50  0000 R CNN
F 2 "" H 31050 1300 50  0001 L CNN
F 3 "" H 31000 1620 50  0001 L CNN
	1    31000 1600
	1    0    0    -1  
$EndComp
$Comp
L 7SEGMENT_CC U?
U 1 1 5A528F1C
P 31950 1600
F 0 "U?" H 31450 1975 50  0000 R CNN
F 1 "7SEGMENT_CC" H 31450 1900 50  0000 R CNN
F 2 "" H 32000 1300 50  0001 L CNN
F 3 "" H 31950 1620 50  0001 L CNN
	1    31950 1600
	1    0    0    -1  
$EndComp
$Comp
L 7SEGMENT_CC U?
U 1 1 5A528F22
P 32900 1600
F 0 "U?" H 32400 1975 50  0000 R CNN
F 1 "7SEGMENT_CC" H 32400 1900 50  0000 R CNN
F 2 "" H 32950 1300 50  0001 L CNN
F 3 "" H 32900 1620 50  0001 L CNN
	1    32900 1600
	1    0    0    -1  
$EndComp
$Comp
L 7SEGMENT_CC U?
U 1 1 5A528FAE
P 33850 1600
F 0 "U?" H 33350 1975 50  0000 R CNN
F 1 "7SEGMENT_CC" H 33350 1900 50  0000 R CNN
F 2 "" H 33900 1300 50  0001 L CNN
F 3 "" H 33850 1620 50  0001 L CNN
	1    33850 1600
	1    0    0    -1  
$EndComp
$Comp
L 7SEGMENT_CC U?
U 1 1 5A528FB4
P 34800 1600
F 0 "U?" H 34300 1975 50  0000 R CNN
F 1 "7SEGMENT_CC" H 34300 1900 50  0000 R CNN
F 2 "" H 34850 1300 50  0001 L CNN
F 3 "" H 34800 1620 50  0001 L CNN
	1    34800 1600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5A529F25
P 30100 2100
F 0 "#PWR?" H 30100 1850 50  0001 C CNN
F 1 "GND" H 30100 1950 50  0000 C CNN
F 2 "" H 30100 2100 50  0001 C CNN
F 3 "" H 30100 2100 50  0001 C CNN
	1    30100 2100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5A529F75
P 31050 2100
F 0 "#PWR?" H 31050 1850 50  0001 C CNN
F 1 "GND" H 31050 1950 50  0000 C CNN
F 2 "" H 31050 2100 50  0001 C CNN
F 3 "" H 31050 2100 50  0001 C CNN
	1    31050 2100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5A529FA1
P 32000 2100
F 0 "#PWR?" H 32000 1850 50  0001 C CNN
F 1 "GND" H 32000 1950 50  0000 C CNN
F 2 "" H 32000 2100 50  0001 C CNN
F 3 "" H 32000 2100 50  0001 C CNN
	1    32000 2100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5A529FE5
P 32950 2100
F 0 "#PWR?" H 32950 1850 50  0001 C CNN
F 1 "GND" H 32950 1950 50  0000 C CNN
F 2 "" H 32950 2100 50  0001 C CNN
F 3 "" H 32950 2100 50  0001 C CNN
	1    32950 2100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5A52A011
P 33900 2100
F 0 "#PWR?" H 33900 1850 50  0001 C CNN
F 1 "GND" H 33900 1950 50  0000 C CNN
F 2 "" H 33900 2100 50  0001 C CNN
F 3 "" H 33900 2100 50  0001 C CNN
	1    33900 2100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5A52A03D
P 34850 2100
F 0 "#PWR?" H 34850 1850 50  0001 C CNN
F 1 "GND" H 34850 1950 50  0000 C CNN
F 2 "" H 34850 2100 50  0001 C CNN
F 3 "" H 34850 2100 50  0001 C CNN
	1    34850 2100
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 5A52A86D
P 31400 1100
F 0 "#PWR?" H 31400 950 50  0001 C CNN
F 1 "+5V" H 31400 1240 50  0000 C CNN
F 2 "" H 31400 1100 50  0001 C CNN
F 3 "" H 31400 1100 50  0001 C CNN
	1    31400 1100
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 5A52A8A5
P 33300 1100
F 0 "#PWR?" H 33300 950 50  0001 C CNN
F 1 "+5V" H 33300 1240 50  0000 C CNN
F 2 "" H 33300 1100 50  0001 C CNN
F 3 "" H 33300 1100 50  0001 C CNN
	1    33300 1100
	1    0    0    -1  
$EndComp
$Comp
L 74LS90 U?
U 1 1 5A52AA86
P 26300 -3550
F 0 "U?" H 26400 -3550 50  0000 C CNN
F 1 "74LS90" H 26500 -3750 50  0000 C CNN
F 2 "" H 26300 -3550 50  0001 C CNN
F 3 "" H 26300 -3550 50  0001 C CNN
	1    26300 -3550
	1    0    0    -1  
$EndComp
$Comp
L 74LS90 U?
U 1 1 5A52AC73
P 26300 -2700
F 0 "U?" H 26400 -2700 50  0000 C CNN
F 1 "74LS90" H 26500 -2900 50  0000 C CNN
F 2 "" H 26300 -2700 50  0001 C CNN
F 3 "" H 26300 -2700 50  0001 C CNN
	1    26300 -2700
	1    0    0    -1  
$EndComp
$Comp
L 74LS90 U?
U 1 1 5A52ADA5
P 26300 -1850
F 0 "U?" H 26400 -1850 50  0000 C CNN
F 1 "74LS90" H 26500 -2050 50  0000 C CNN
F 2 "" H 26300 -1850 50  0001 C CNN
F 3 "" H 26300 -1850 50  0001 C CNN
	1    26300 -1850
	1    0    0    -1  
$EndComp
$Comp
L 74LS90 U?
U 1 1 5A52ADAB
P 26300 -1000
F 0 "U?" H 26400 -1000 50  0000 C CNN
F 1 "74LS90" H 26500 -1200 50  0000 C CNN
F 2 "" H 26300 -1000 50  0001 C CNN
F 3 "" H 26300 -1000 50  0001 C CNN
	1    26300 -1000
	1    0    0    -1  
$EndComp
$Comp
L 74LS90 U?
U 1 1 5A52ADD1
P 26300 -150
F 0 "U?" H 26400 -150 50  0000 C CNN
F 1 "74LS90" H 26500 -350 50  0000 C CNN
F 2 "" H 26300 -150 50  0001 C CNN
F 3 "" H 26300 -150 50  0001 C CNN
	1    26300 -150
	1    0    0    -1  
$EndComp
$Comp
L 74LS90 U?
U 1 1 5A52AE76
P 26300 700
F 0 "U?" H 26400 700 50  0000 C CNN
F 1 "74LS90" H 26500 500 50  0000 C CNN
F 2 "" H 26300 700 50  0001 C CNN
F 3 "" H 26300 700 50  0001 C CNN
	1    26300 700 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5A52D338
P 25200 -3500
F 0 "#PWR?" H 25200 -3750 50  0001 C CNN
F 1 "GND" H 25200 -3650 50  0000 C CNN
F 2 "" H 25200 -3500 50  0001 C CNN
F 3 "" H 25200 -3500 50  0001 C CNN
	1    25200 -3500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5A52D41D
P 25200 -2650
F 0 "#PWR?" H 25200 -2900 50  0001 C CNN
F 1 "GND" H 25200 -2800 50  0000 C CNN
F 2 "" H 25200 -2650 50  0001 C CNN
F 3 "" H 25200 -2650 50  0001 C CNN
	1    25200 -2650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5A52D453
P 25200 -1800
F 0 "#PWR?" H 25200 -2050 50  0001 C CNN
F 1 "GND" H 25200 -1950 50  0000 C CNN
F 2 "" H 25200 -1800 50  0001 C CNN
F 3 "" H 25200 -1800 50  0001 C CNN
	1    25200 -1800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5A52D463
P 25200 -950
F 0 "#PWR?" H 25200 -1200 50  0001 C CNN
F 1 "GND" H 25200 -1100 50  0000 C CNN
F 2 "" H 25200 -950 50  0001 C CNN
F 3 "" H 25200 -950 50  0001 C CNN
	1    25200 -950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5A52D49B
P 25200 -100
F 0 "#PWR?" H 25200 -350 50  0001 C CNN
F 1 "GND" H 25200 -250 50  0000 C CNN
F 2 "" H 25200 -100 50  0001 C CNN
F 3 "" H 25200 -100 50  0001 C CNN
	1    25200 -100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5A52D4AB
P 25200 750
F 0 "#PWR?" H 25200 500 50  0001 C CNN
F 1 "GND" H 25200 600 50  0000 C CNN
F 2 "" H 25200 750 50  0001 C CNN
F 3 "" H 25200 750 50  0001 C CNN
	1    25200 750 
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 5A52D6F5
P 27800 -4050
F 0 "#PWR?" H 27800 -4200 50  0001 C CNN
F 1 "+5V" H 27800 -3910 50  0000 C CNN
F 2 "" H 27800 -4050 50  0001 C CNN
F 3 "" H 27800 -4050 50  0001 C CNN
	1    27800 -4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	29300 1000 30250 1000
Wire Wire Line
	30250 1000 30250 1200
Wire Wire Line
	29300 900  30150 900 
Wire Wire Line
	30150 900  30150 1200
Wire Wire Line
	29300 800  30050 800 
Wire Wire Line
	30050 800  30050 1200
Wire Wire Line
	29300 700  29950 700 
Wire Wire Line
	29950 700  29950 1200
Wire Wire Line
	29300 600  29850 600 
Wire Wire Line
	29850 600  29850 1200
Wire Wire Line
	29300 500  29750 500 
Wire Wire Line
	29750 500  29750 1200
Wire Wire Line
	29300 400  29650 400 
Wire Wire Line
	29650 400  29650 1200
Wire Wire Line
	29300 150  31200 150 
Wire Wire Line
	31200 150  31200 1200
Wire Wire Line
	29300 50   31100 50  
Wire Wire Line
	31100 50   31100 1200
Wire Wire Line
	29300 -50  31000 -50 
Wire Wire Line
	31000 -50  31000 1200
Wire Wire Line
	29300 -150 30900 -150
Wire Wire Line
	30900 -150 30900 1200
Wire Wire Line
	29300 -250 30800 -250
Wire Wire Line
	30800 -250 30800 1200
Wire Wire Line
	29300 -350 30700 -350
Wire Wire Line
	30700 -350 30700 1200
Wire Wire Line
	29300 -450 30600 -450
Wire Wire Line
	30600 -450 30600 1200
Wire Wire Line
	29300 -700 32150 -700
Wire Wire Line
	32150 -700 32150 1200
Wire Wire Line
	32050 1200 32050 -800
Wire Wire Line
	32050 -800 29300 -800
Wire Wire Line
	29300 -900 31950 -900
Wire Wire Line
	31950 -900 31950 1200
Wire Wire Line
	31850 1200 31850 -1000
Wire Wire Line
	31850 -1000 29300 -1000
Wire Wire Line
	29300 -1100 31750 -1100
Wire Wire Line
	31750 -1100 31750 1200
Wire Wire Line
	31650 1200 31650 -1200
Wire Wire Line
	31650 -1200 29300 -1200
Wire Wire Line
	29300 -1300 31550 -1300
Wire Wire Line
	31550 -1300 31550 1200
Wire Wire Line
	33100 1200 33100 -1550
Wire Wire Line
	33100 -1550 29300 -1550
Wire Wire Line
	29300 -1650 33000 -1650
Wire Wire Line
	33000 -1650 33000 1200
Wire Wire Line
	32900 1200 32900 -1750
Wire Wire Line
	32900 -1750 29300 -1750
Wire Wire Line
	29300 -1850 32800 -1850
Wire Wire Line
	32800 -1850 32800 1200
Wire Wire Line
	32700 1200 32700 -1950
Wire Wire Line
	32700 -1950 29300 -1950
Wire Wire Line
	29300 -2050 32600 -2050
Wire Wire Line
	32600 -2050 32600 1200
Wire Wire Line
	32500 1200 32500 -2150
Wire Wire Line
	32500 -2150 29300 -2150
Wire Wire Line
	29300 -2400 34050 -2400
Wire Wire Line
	34050 -2400 34050 1200
Wire Wire Line
	33950 1200 33950 -2500
Wire Wire Line
	33950 -2500 29300 -2500
Wire Wire Line
	29300 -2600 33850 -2600
Wire Wire Line
	33850 -2600 33850 1200
Wire Wire Line
	33750 1200 33750 -2700
Wire Wire Line
	33750 -2700 29300 -2700
Wire Wire Line
	29300 -2800 33650 -2800
Wire Wire Line
	33650 -2800 33650 1200
Wire Wire Line
	33550 1200 33550 -2900
Wire Wire Line
	33550 -2900 29300 -2900
Wire Wire Line
	29300 -3000 33450 -3000
Wire Wire Line
	33450 -3000 33450 1200
Wire Wire Line
	29300 -3250 35000 -3250
Wire Wire Line
	35000 -3250 35000 1200
Wire Wire Line
	34900 1200 34900 -3350
Wire Wire Line
	34900 -3350 29300 -3350
Wire Wire Line
	29300 -3450 34800 -3450
Wire Wire Line
	34800 -3450 34800 1200
Wire Wire Line
	34700 1200 34700 -3550
Wire Wire Line
	34700 -3550 29300 -3550
Wire Wire Line
	29300 -3650 34600 -3650
Wire Wire Line
	34600 -3650 34600 1200
Wire Wire Line
	34500 1200 34500 -3750
Wire Wire Line
	34500 -3750 29300 -3750
Wire Wire Line
	29300 -3850 34400 -3850
Wire Wire Line
	34400 -3850 34400 1200
Wire Wire Line
	30050 2000 30050 2050
Wire Wire Line
	30050 2050 30100 2050
Wire Wire Line
	30100 2050 30150 2050
Wire Wire Line
	30100 2050 30100 2100
Wire Wire Line
	30150 2050 30150 2000
Connection ~ 30100 2050
Wire Wire Line
	31000 2000 31000 2050
Wire Wire Line
	31000 2050 31050 2050
Wire Wire Line
	31050 2050 31100 2050
Wire Wire Line
	31050 2050 31050 2100
Wire Wire Line
	31100 2050 31100 2000
Connection ~ 31050 2050
Wire Wire Line
	31950 2000 31950 2050
Wire Wire Line
	31950 2050 32000 2050
Wire Wire Line
	32000 2050 32050 2050
Wire Wire Line
	32000 2050 32000 2100
Wire Wire Line
	32050 2050 32050 2000
Connection ~ 32000 2050
Wire Wire Line
	32900 2000 32900 2050
Wire Wire Line
	32900 2050 32950 2050
Wire Wire Line
	32950 2050 33000 2050
Wire Wire Line
	32950 2050 32950 2100
Wire Wire Line
	33000 2050 33000 2000
Connection ~ 32950 2050
Wire Wire Line
	33850 2000 33850 2050
Wire Wire Line
	33850 2050 33900 2050
Wire Wire Line
	33900 2050 33950 2050
Wire Wire Line
	33900 2050 33900 2100
Wire Wire Line
	33950 2050 33950 2000
Connection ~ 33900 2050
Wire Wire Line
	34800 2000 34800 2050
Wire Wire Line
	34800 2050 34850 2050
Wire Wire Line
	34850 2050 34900 2050
Wire Wire Line
	34850 2050 34850 2100
Wire Wire Line
	34900 2050 34900 2000
Connection ~ 34850 2050
Wire Wire Line
	33300 1100 33300 1200
Wire Wire Line
	31400 1100 31400 1200
Wire Wire Line
	27050 400  27150 400 
Wire Wire Line
	27150 400  27900 400 
Wire Wire Line
	27050 500  27900 500 
Wire Wire Line
	27050 600  27900 600 
Wire Wire Line
	27050 700  27900 700 
Wire Wire Line
	27050 -450 27150 -450
Wire Wire Line
	27150 -450 27900 -450
Wire Wire Line
	27050 -350 27900 -350
Wire Wire Line
	27900 -250 27050 -250
Wire Wire Line
	27050 -150 27900 -150
Wire Wire Line
	27050 -1300 27150 -1300
Wire Wire Line
	27150 -1300 27900 -1300
Wire Wire Line
	27900 -1200 27050 -1200
Wire Wire Line
	27050 -1100 27900 -1100
Wire Wire Line
	27900 -1000 27050 -1000
Wire Wire Line
	27050 -2150 27150 -2150
Wire Wire Line
	27150 -2150 27900 -2150
Wire Wire Line
	27900 -2050 27050 -2050
Wire Wire Line
	27050 -1950 27900 -1950
Wire Wire Line
	27050 -1850 27900 -1850
Wire Wire Line
	27050 -3000 27150 -3000
Wire Wire Line
	27150 -3000 27900 -3000
Wire Wire Line
	27050 -2900 27900 -2900
Wire Wire Line
	27900 -2800 27050 -2800
Wire Wire Line
	27050 -2700 27900 -2700
Wire Wire Line
	27050 -3850 27150 -3850
Wire Wire Line
	27150 -3850 27900 -3850
Wire Wire Line
	27900 -3750 27050 -3750
Wire Wire Line
	27050 -3650 27900 -3650
Wire Wire Line
	27900 -3550 27050 -3550
Wire Wire Line
	25600 500  25500 500 
Wire Wire Line
	25500 500  25500 250 
Wire Wire Line
	25500 250  27150 250 
Wire Wire Line
	27150 250  27150 400 
Connection ~ 27150 400 
Wire Wire Line
	25500 -350 25600 -350
Wire Wire Line
	25500 -600 25500 -350
Wire Wire Line
	25500 -600 27150 -600
Wire Wire Line
	27150 -600 27150 -450
Connection ~ 27150 -450
Wire Wire Line
	25600 -1200 25500 -1200
Wire Wire Line
	25500 -1200 25500 -1450
Wire Wire Line
	25500 -1450 27150 -1450
Wire Wire Line
	27150 -1450 27150 -1300
Connection ~ 27150 -1300
Wire Wire Line
	25600 -2050 25500 -2050
Wire Wire Line
	25500 -2050 25500 -2300
Wire Wire Line
	25500 -2300 27150 -2300
Wire Wire Line
	27150 -2300 27150 -2150
Connection ~ 27150 -2150
Wire Wire Line
	25600 -2900 25500 -2900
Wire Wire Line
	25500 -2900 25500 -3150
Wire Wire Line
	25500 -3150 27150 -3150
Wire Wire Line
	27150 -3150 27150 -3000
Connection ~ 27150 -3000
Wire Wire Line
	25600 -3750 25500 -3750
Wire Wire Line
	25500 -3750 25500 -4000
Wire Wire Line
	25500 -4000 27150 -4000
Wire Wire Line
	27150 -4000 27150 -3850
Connection ~ 27150 -3850
Wire Wire Line
	25200 -3600 25350 -3600
Wire Wire Line
	25350 -3600 25600 -3600
Wire Wire Line
	25350 -3600 25350 -3500
Wire Wire Line
	25350 -3500 25350 -3350
Wire Wire Line
	25350 -3350 25350 -3250
Wire Wire Line
	25600 -3500 25350 -3500
Connection ~ 25350 -3500
Wire Wire Line
	25600 -3350 25350 -3350
Connection ~ 25350 -3350
Wire Wire Line
	25350 -3250 25600 -3250
Connection ~ 25350 -3250
Wire Wire Line
	25200 -3600 25200 -3500
Connection ~ 25350 -3600
Wire Wire Line
	25200 -2750 25350 -2750
Wire Wire Line
	25350 -2750 25600 -2750
Wire Wire Line
	25350 -2750 25350 -2650
Wire Wire Line
	25350 -2650 25350 -2500
Wire Wire Line
	25350 -2500 25350 -2400
Wire Wire Line
	25600 -2650 25350 -2650
Connection ~ 25350 -2650
Wire Wire Line
	25600 -2500 25350 -2500
Connection ~ 25350 -2500
Wire Wire Line
	25350 -2400 25600 -2400
Connection ~ 25350 -2400
Wire Wire Line
	25200 -2750 25200 -2650
Connection ~ 25350 -2750
Wire Wire Line
	25200 -1900 25350 -1900
Wire Wire Line
	25350 -1900 25600 -1900
Wire Wire Line
	25350 -1900 25350 -1800
Wire Wire Line
	25350 -1800 25350 -1650
Wire Wire Line
	25350 -1650 25350 -1550
Wire Wire Line
	25600 -1800 25350 -1800
Connection ~ 25350 -1800
Wire Wire Line
	25600 -1650 25350 -1650
Connection ~ 25350 -1650
Wire Wire Line
	25350 -1550 25600 -1550
Connection ~ 25350 -1550
Wire Wire Line
	25200 -1900 25200 -1800
Connection ~ 25350 -1900
Wire Wire Line
	25200 -1050 25350 -1050
Wire Wire Line
	25350 -1050 25600 -1050
Wire Wire Line
	25350 -1050 25350 -950
Wire Wire Line
	25350 -950 25350 -800
Wire Wire Line
	25350 -800 25350 -700
Wire Wire Line
	25600 -950 25350 -950
Connection ~ 25350 -950
Wire Wire Line
	25600 -800 25350 -800
Connection ~ 25350 -800
Wire Wire Line
	25350 -700 25600 -700
Connection ~ 25350 -700
Wire Wire Line
	25200 -1050 25200 -950
Connection ~ 25350 -1050
Wire Wire Line
	25200 -200 25350 -200
Wire Wire Line
	25350 -200 25600 -200
Wire Wire Line
	25350 -200 25350 -100
Wire Wire Line
	25350 -100 25350 50  
Wire Wire Line
	25350 50   25350 150 
Wire Wire Line
	25600 -100 25350 -100
Connection ~ 25350 -100
Wire Wire Line
	25600 50   25350 50  
Connection ~ 25350 50  
Wire Wire Line
	25350 150  25600 150 
Connection ~ 25350 150 
Wire Wire Line
	25200 -200 25200 -100
Connection ~ 25350 -200
Wire Wire Line
	25200 650  25350 650 
Wire Wire Line
	25350 650  25600 650 
Wire Wire Line
	25350 650  25350 750 
Wire Wire Line
	25350 750  25350 900 
Wire Wire Line
	25350 900  25350 1000
Wire Wire Line
	25600 750  25350 750 
Connection ~ 25350 750 
Wire Wire Line
	25600 900  25350 900 
Connection ~ 25350 900 
Wire Wire Line
	25350 1000 25600 1000
Connection ~ 25350 1000
Wire Wire Line
	25200 650  25200 750 
Connection ~ 25350 650 
Wire Wire Line
	27900 900  27800 900 
Wire Wire Line
	27800 -4050 27800 -3350
Wire Wire Line
	27800 -3350 27800 -3250
Wire Wire Line
	27800 -3250 27800 -3150
Wire Wire Line
	27800 -3150 27800 -2500
Wire Wire Line
	27800 -2500 27800 -2400
Wire Wire Line
	27800 -2400 27800 -2300
Wire Wire Line
	27800 -2300 27800 -1650
Wire Wire Line
	27800 -1650 27800 -1550
Wire Wire Line
	27800 -1550 27800 -1450
Wire Wire Line
	27800 -1450 27800 -800
Wire Wire Line
	27800 -800 27800 -700
Wire Wire Line
	27800 -700 27800 -600
Wire Wire Line
	27800 -600 27800 50  
Wire Wire Line
	27800 50   27800 150 
Wire Wire Line
	27800 150  27800 250 
Wire Wire Line
	27800 250  27800 900 
Wire Wire Line
	27800 900  27800 1000
Wire Wire Line
	27800 1000 27800 1100
Wire Wire Line
	27900 -3350 27800 -3350
Connection ~ 27800 -3350
Wire Wire Line
	27900 -2500 27800 -2500
Connection ~ 27800 -2500
Wire Wire Line
	27900 -1650 27800 -1650
Connection ~ 27800 -1650
Wire Wire Line
	27900 -800 27800 -800
Connection ~ 27800 -800
Wire Wire Line
	27900 50   27800 50  
Connection ~ 27800 50  
Wire Wire Line
	27800 1100 27900 1100
Connection ~ 27800 900 
Wire Wire Line
	27900 1000 27800 1000
Connection ~ 27800 1000
Wire Wire Line
	27900 150  27800 150 
Connection ~ 27800 150 
Wire Wire Line
	27900 250  27800 250 
Connection ~ 27800 250 
Wire Wire Line
	27900 -600 27800 -600
Connection ~ 27800 -600
Wire Wire Line
	27900 -700 27800 -700
Connection ~ 27800 -700
Wire Wire Line
	27900 -1450 27800 -1450
Connection ~ 27800 -1450
Wire Wire Line
	27900 -1550 27800 -1550
Connection ~ 27800 -1550
Wire Wire Line
	27900 -2300 27800 -2300
Connection ~ 27800 -2300
Wire Wire Line
	27900 -2400 27800 -2400
Connection ~ 27800 -2400
Wire Wire Line
	27900 -3150 27800 -3150
Connection ~ 27800 -3150
Wire Wire Line
	27900 -3250 27800 -3250
Connection ~ 27800 -3250
Wire Wire Line
	32350 1200 32350 700 
Wire Wire Line
	32350 700  32250 700 
Wire Wire Line
	32250 700  32250 800 
$Comp
L GND #PWR?
U 1 1 5A52EF63
P 32250 800
F 0 "#PWR?" H 32250 550 50  0001 C CNN
F 1 "GND" H 32250 650 50  0000 C CNN
F 2 "" H 32250 800 50  0001 C CNN
F 3 "" H 32250 800 50  0001 C CNN
	1    32250 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	30450 1200 30450 700 
Wire Wire Line
	30450 700  30350 700 
Wire Wire Line
	30350 700  30350 800 
$Comp
L GND #PWR?
U 1 1 5A52F09B
P 30350 800
F 0 "#PWR?" H 30350 550 50  0001 C CNN
F 1 "GND" H 30350 650 50  0000 C CNN
F 2 "" H 30350 800 50  0001 C CNN
F 3 "" H 30350 800 50  0001 C CNN
	1    30350 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	34250 1200 34250 700 
Wire Wire Line
	34250 700  34150 700 
Wire Wire Line
	34150 700  34150 800 
$Comp
L GND #PWR?
U 1 1 5A52F0CA
P 34150 800
F 0 "#PWR?" H 34150 550 50  0001 C CNN
F 1 "GND" H 34150 650 50  0000 C CNN
F 2 "" H 34150 800 50  0001 C CNN
F 3 "" H 34150 800 50  0001 C CNN
	1    34150 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	35200 1200 35200 700 
Wire Wire Line
	35200 700  35100 700 
Wire Wire Line
	35100 700  35100 800 
$Comp
L GND #PWR?
U 1 1 5A52F189
P 35100 800
F 0 "#PWR?" H 35100 550 50  0001 C CNN
F 1 "GND" H 35100 650 50  0000 C CNN
F 2 "" H 35100 800 50  0001 C CNN
F 3 "" H 35100 800 50  0001 C CNN
	1    35100 800 
	1    0    0    -1  
$EndComp
$EndSCHEMATC

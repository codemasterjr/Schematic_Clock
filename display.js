class SevenSegment {
  constructor(posX, posY, name) {
    this.wDisplay = widthDisplay;
    this.hDisplay = heightDisplay;
    this.pinCount = 10;
    this.offSet = 35
    this.pinDim = pinDimension;
    this.pinList = [];
    this.segmentA = new Segment();
    this.segmentB = new Segment();
    this.segmentC = new Segment();
    this.segmentD = new Segment();
    this.segmentE = new Segment();
    this.segmentF = new Segment();
    this.segmentG = new Segment();
    this.dot = new LED();
    this.x = posX;
    this.y = posY;
    for (let i = 0; i < this.pinCount / 2; i++) {
      let xc = new Pin(i + 1, this.x + (this.pinDim / 2) + (i * this.pinDim * 3) + this.offSet, this.y + (this.pinDim / 2) + this.hDisplay);
      this.pinList.push(xc);
    }
    for (let i = this.pinCount / 2; i < this.pinCount; i++) {
      let xc = new Pin(i + 1, this.x + this.wDisplay - (this.pinDim / 2) - ((i - (this.pinCount / 2)) * this.pinDim * 3) - this.offSet, this.y - (this.pinDim / 2));
      this.pinList.push(xc);
    }
    this.pinList[0].pinName = name + "_e";
    this.pinList[1].pinName = name + "_d";
    this.pinList[2].pinName = "COM";
    this.pinList[3].pinName = name + "_c";
    this.pinList[4].pinName = name + "_P";
    this.pinList[5].pinName = name + "_b";
    this.pinList[6].pinName = name + "_a";
    this.pinList[7].pinName = "COM";
    this.pinList[8].pinName = name + "_f";
    this.pinList[9].pinName = name + "_g";
  }
  show() {
    rectMode(CORNER);
    stroke(0);
    fill(205, 197, 191);
    rect(this.x, this.y, this.wDisplay, this.hDisplay);
    fill(0);
    rect(this.x + 4, this.y + 4, this.wDisplay - 8, this.hDisplay - 8);

    //Placement des segements
    push();
    translate(this.x, this.y);
    translate((this.wDisplay / 2), 32.5);
    this.segmentA.show();
    translate(lenghtSegment / 2, lenghtSegment / 2);
    push();
    rotate(HALF_PI);
    this.segmentB.show();
    pop();
    push();
    translate(30, -5);
    this.dot.show();
    pop();
    translate(0, lenghtSegment);
    push();
    rotate(HALF_PI);
    this.segmentC.show();
    pop();
    push();
    translate(30, 5);
    this.dot.show();
    pop();
    translate(-lenghtSegment / 2, lenghtSegment / 2);
    this.segmentD.show();
    translate(-lenghtSegment / 2, -lenghtSegment / 2)
    push();
    rotate(HALF_PI);
    this.segmentE.show();
    pop();
    translate(0, -lenghtSegment)
    push();
    rotate(HALF_PI);
    this.segmentF.show();
    pop();
    translate(lenghtSegment / 2, lenghtSegment / 2);
    this.segmentG.show();
    pop();

    //Placement des pins
    for (let i = 0; i < this.pinList.length; i++) {
      this.pinList[i].show();
    }
  }

  displayNumber(number, unit) {
    if (unit) {
      this.dot.state = 1;
    } else {
      this.dot.state = 0;
    }
    switch (number) {
      case 0:
        this.segmentA.state = 1;
        this.segmentB.state = 1;
        this.segmentC.state = 1;
        this.segmentD.state = 1;
        this.segmentE.state = 1;
        this.segmentF.state = 1;
        this.segmentG.state = 0;
        break;
      case 1:
        this.segmentA.state = 0;
        this.segmentB.state = 1;
        this.segmentC.state = 1;
        this.segmentD.state = 0;
        this.segmentE.state = 0;
        this.segmentF.state = 0;
        this.segmentG.state = 0;
        break;
      case 2:
        this.segmentA.state = 1;
        this.segmentB.state = 1;
        this.segmentC.state = 0;
        this.segmentD.state = 1;
        this.segmentE.state = 1;
        this.segmentF.state = 0;
        this.segmentG.state = 1;
        break;
      case 3:
        this.segmentA.state = 1;
        this.segmentB.state = 1;
        this.segmentC.state = 1;
        this.segmentD.state = 1;
        this.segmentE.state = 0;
        this.segmentF.state = 0;
        this.segmentG.state = 1;
        break;
      case 4:
        this.segmentA.state = 0;
        this.segmentB.state = 1;
        this.segmentC.state = 1;
        this.segmentD.state = 0;
        this.segmentE.state = 0;
        this.segmentF.state = 1;
        this.segmentG.state = 1;
        break;
      case 5:
        this.segmentA.state = 1;
        this.segmentB.state = 0;
        this.segmentC.state = 1;
        this.segmentD.state = 1;
        this.segmentE.state = 0;
        this.segmentF.state = 1;
        this.segmentG.state = 1;
        break;
      case 6:
        this.segmentA.state = 0;
        this.segmentB.state = 0;
        this.segmentC.state = 1;
        this.segmentD.state = 1;
        this.segmentE.state = 1;
        this.segmentF.state = 1;
        this.segmentG.state = 1;
        break;
      case 7:
        this.segmentA.state = 1;
        this.segmentB.state = 1;
        this.segmentC.state = 1;
        this.segmentD.state = 0;
        this.segmentE.state = 0;
        this.segmentF.state = 0;
        this.segmentG.state = 0;
        break;
      case 8:
        this.segmentA.state = 1;
        this.segmentB.state = 1;
        this.segmentC.state = 1;
        this.segmentD.state = 1;
        this.segmentE.state = 1;
        this.segmentF.state = 1;
        this.segmentG.state = 1;
        break;
      case 9:
        this.segmentA.state = 1;
        this.segmentB.state = 1;
        this.segmentC.state = 1;
        this.segmentD.state = 0;
        this.segmentE.state = 0;
        this.segmentF.state = 1;
        this.segmentG.state = 1;
        break;
    }
  }
}
/*
BCD 74LS48 http://pdf.datasheetcatalog.com/datasheets/700/338023_DS.pdf
  1 -|B      VCC|- 16
  2 -|C        f|- 15
  3 -|/TL      g|- 14
  4 -|/BI,/RBO a|- 13
  5 -|/RBI     b|- 12
  6 -|D        c|- 11
  7 -|A        d|- 10
  8 -|GND      e|-  9
*/
"use strict";
const pin_Vdd = 2;
const pin_in = 1;
const pin_out = 0;

class Dip {
  constructor(numberOfPin, posX, posY, name) {
    this.offset = 10;
    this.pinDim = pinDimension;
    this.pinCount = numberOfPin;
    this.dipHeight;
    this.pinList = [];
    this.dipWidth;
    this.pinName;
    this.name = name;
    this.x = posX;
    this.y = posY;

    this.dipWidth = 80;
    this.dipHeight = ((this.pinCount / 2) * (this.pinDim * 2)) + (this.offset * 2) - this.pinDim;
    for (let i = 0; i < this.pinCount / 2; i++) {
      let xc = new Pin(i + 1, this.x - (this.pinDim / 2), this.y + this.offset + (this.pinDim / 2) + (i * this.pinDim * 2));
      this.pinList.push(xc);
    }
    for (let i = this.pinCount / 2; i < this.pinCount; i++) {
      let xc = new Pin(i + 1, this.x + (this.pinDim / 2) + this.dipWidth, this.y + this.dipHeight - this.offset - (this.pinDim / 2) - ((i - (this.pinCount / 2)) * this.pinDim * 2));
      this.pinList.push(xc);
    }
  }

  show() {
    rectMode(CORNER);
    noStroke();
    fill(50);
    rect(this.x, this.y, this.dipWidth, this.dipHeight);
    fill(80);
    arc(this.x + (this.dipWidth / 2), this.y, this.dipWidth / 2, this.dipWidth / 2, 0, PI);
    push();
    translate(this.x + (this.dipWidth / 2), this.y + (this.dipHeight / 2));
    rotate(HALF_PI);
    textAlign(CENTER, CENTER);
    fill(150);
    textSize(20);
    textFont('Consolas');
    text(this.name, 0, 0);
    pop();
    for (let i = 0; i < this.pinList.length; i++) {
      this.pinList[i].show();
    }
  }
}